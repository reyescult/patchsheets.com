/*global require*/

(function () {
    
    'use strict';
    
    // Requires
    
    var gulp = require('gulp'),
        browserSync = require('browser-sync').create(),
        sass = require('gulp-sass'),
        useref = require('gulp-useref'),
        gulpIf = require('gulp-if'),
        cssnano = require('gulp-cssnano'),
        uglify = require('gulp-uglify'),
        cache = require('gulp-cache'),
        imagemin = require('gulp-imagemin'),
        del = require('del'),
        runSequence = require('run-sequence');
    
    // Browser Sync
    
    gulp.task('browserSync', function () {
        browserSync.init({
            server: {
                baseDir: 'src'
            }
        });
    });
    
    // Compile SASS
    
    gulp.task('sass', function () {
        return gulp.src('src/assets/scss/**/*.scss')
            .pipe(sass())
            .pipe(gulp.dest('src/assets/css'))
            .pipe(browserSync.reload({
                stream: true
            }));
    });
    
    // Concatenate & Minify CSS & JS
    
    gulp.task('useref', function () {
        return gulp.src('src/*.html')
            .pipe(useref())
            .pipe(gulpIf('*.css', cssnano()))
            .pipe(gulpIf('*.js', uglify()))
            .pipe(gulp.dest('dist'));
    });
    
    // Optimize Images
    
    gulp.task('images', function () {
        return gulp.src('src/assets/img/**/*.+(png|jpg|gif|svg)')
            .pipe(cache(imagemin()))
            .pipe(gulp.dest('dist/assets/img'));
    });
    
    // Copy Fonts
    
    gulp.task('fonts', function () {
        return gulp.src('src/assets/fonts/**/*')
            .pipe(gulp.dest('dist/assets/fonts'));
    });
    
    // Copy HTML
    
    gulp.task('html', function () {
        return gulp.src(['src/**/*.html', '!src/index.html'])
            .pipe(gulp.dest('dist'));
    });
    
    // Copy JSON
    
    gulp.task('json', function () {
        return gulp.src('src/**/*.json')
            .pipe(gulp.dest('dist'));
    });
    
    // Clear Cache
    
    gulp.task('clear', function (callback) {
        return cache.clearAll(callback);
    });
    
    // Clean Dist
    
    gulp.task('clean', function () {
        return del.sync('dist');
    });
    
    // Build Dist
    
    gulp.task('build', function (callback) {
        runSequence('clean', 'sass', ['useref', 'images', 'fonts', 'html', 'json'], callback);
    });
    
    // Watchers
    
    gulp.task('watch', ['browserSync', 'sass'], function () {
        gulp.watch('src/**/*.html', browserSync.reload); // Watch HTML
        gulp.watch('src/**/*.js', browserSync.reload); // Watch JS
        gulp.watch('src/**/*.json', browserSync.reload); // Watch JSON
        gulp.watch('src/assets/scss/**/*.scss', ['sass']); // Watch SASS
    });
    
    // Default
    
    gulp.task('default', function (callback) {
        runSequence(['browserSync', 'sass', 'watch'], callback);
    });
    
}());