# patchsheets.com

This is the Patchsheets.com AngularJS (version 1.6.x) project. This application is a digital patchsheet respository for synthesizers with no internal patch memory. It can be used to store snapshots of synthesizer setting for later recall.

###Gulp Tasks

**gulp**

Default task for development

*This task runs 'browserSync', 'sass' and 'watch' in parallel.*

**gulp browserSync**

Spin up a web server with live reload for development

**gulp sass**

Compile SASS to CSS for development

**gulp useref**

Concatenates and minifies CSS & JS for distribution (dist)

**gulp images**

Optimizes images for distribution (dist)

**gulp fonts**

Copies fonts for distribution (dist)

**gulp html**

Copies HTML files for distribution (dist)

**gulp clear**

Clear the locally cached images

*The cache plugin is used by the image optimization task.*

**gulp clean**

Deletes the local distribution (dist) directory

**gulp build**

Builds the the project for distribution (dist)

*This task runs 'clean' and 'sass' in series, then 'useref', 'images', 'fonts' and 'html' in parallel.*

**gulp watch**

Watches HTML, JS and SASS for changes and refreshes the browser with browserSync if changes are detected