(function () {

    'use strict';

    angular.module('app').component('yamahaCs15', {
        templateUrl: 'components/yamaha-cs-15/yamaha-cs-15.html',
        controller: 'cs15Controller'
    });
    
})();