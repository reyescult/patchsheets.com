(function () {

    'use strict';

    angular.module('app').controller('cs15Controller', ['$scope', 'patchesService', 'cs15PatchService', function ($scope, patchesService, cs15PatchService) {
        var vm = this;
        vm.device = {
            brand: 'Yamaha',
            model: 'CS-15'
        };
        vm.patches = cs15PatchService.patches;
        vm.$onInit = function () {
            patchesService.setPatches(vm.patches);
            patchesService.setCurrent(0);
        };
        vm.$onDestroy = function () {
            patchesService.setPatches([]);
            patchesService.setCurrent({});
        };
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
            vm.editObj = JSON.parse(JSON.stringify(vm.current));
        });
    }]);

})();