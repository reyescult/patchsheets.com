(function () {

    'use strict';

    angular.module('app').service('cs15PatchService', [function () {
        return {
            "patches" : [
                {
                    "settings" : {
                        "author" : "Rick Reyes",
                        "date" : "1483918937642",
                        "category" : "None",
                        "name" : "CS-15 Init Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Sam Jones",
                        "date" : "1483918937642",
                        "category" : "Bass",
                        "name" : "CS-15 Bass Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Eddie Van Halen",
                        "date" : "1483918937642",
                        "category" : "Lead",
                        "name" : "CS-15 Lead Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Chick Corea",
                        "date" : "1483918937642",
                        "category" : "None",
                        "name" : "CS-15 Keys Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Vangelis",
                        "date" : "1483918937642",
                        "category" : "None",
                        "name" : "CS-15 Pad Patch",
                        "locked" : true
                    }
                }
            ]
        };
    }]);

})();