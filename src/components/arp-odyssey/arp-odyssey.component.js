(function () {

    'use strict';

    angular.module('app').component('arpOdyssey', {
        templateUrl: 'components/arp-odyssey/arp-odyssey.html',
        controller: 'odysseyController'
    });
    
})();