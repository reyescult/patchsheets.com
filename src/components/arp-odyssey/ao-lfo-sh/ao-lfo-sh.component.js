(function () {

    'use strict';

    angular.module('app').component('aoLfoSh', {
        templateUrl: 'components/arp-odyssey/ao-lfo-sh/ao-lfo-sh.html',
        controller: 'aoLfoShController',
        bindings: {
            current: '<'
        }
    });
    
})();