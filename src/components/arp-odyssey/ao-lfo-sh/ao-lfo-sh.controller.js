(function () {

    'use strict';

    angular.module('app').controller('aoLfoShController', [function () {

        var vm = this;

        vm.freq = {
            stepsArray: [
                {value: 0.2, legend: '.2Hz'}, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1,
                1.06, 1.12, 1.19, 1.25, 1.3, 1.37, 1.43, 1.5, 1.56, 1.62, 1.69, 1.74, 1.80, 1.87, 1.93, {value: 2, legend: '2Hz'},
                2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10,
                10.63, 11.25, 11.88, 12.5, 13.13, 13.75, 14.38, 15, 15.63, 16.25, 16.88, 17.5, 18.13, 18.75, 19.36, {value: 20, legend: '20Hz'}
            ],
            showSelectionBar: true,
            showTicks: 16, // Should be 16.25, but legends do not show up with floating value.
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.mod = {
            floor: 0,
            ceil: 100,
            showSelectionBar: true,
            showTicks: 25,
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

    }]);
    
})();