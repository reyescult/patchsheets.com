(function () {

    'use strict';

    angular.module('app').service('odysseyPatchService', [function () {
        return {
            "patches" : [
                {
                    "settings" : {
                        "author" : "Rick Reyes",
                        "date" : "1483918715021",
                        "category" : "None",
                        "name" : "Odyssey Init Patch",
                        "locked" : true
                    },
                    "vco1" : {
                        "course" : 200,
                        "fine" : 0,
                        "kybd" : "On",
                        "fm1Amt" : 0,
                        "fm1Src" : "Sine",
                        "fm2Amt" : 0,
                        "fm2Src" : "S/H",
                        "width" : 50,
                        "mod" : 0,
                        "pwSrc" : "LFO"
                    },
                    "vco2" : {
                        "course" : 200,
                        "fine" : 0,
                        "sync" : "OFF",
                        "fm1Amt" : 0,
                        "fm1Src" : "LFO",
                        "fm2Amt" : 0,
                        "fm2Src" : "S/H",
                        "width" : 50,
                        "mod" : 0,
                        "pwSrc" : "LFO"
                    },
                    "lfoFreq" : 2,
                    "sHold" : {
                        "mx1Amt" : 0,
                        "mx1Src" : 'VCO-1 SAW',
                        "mx2Amt" : 0,
                        "mx2Src" : 'NOISE GEN',
                        "trigSrc" : 'LFO TRIG',
                        "lag" : 0
                    }
                },
                {
                    "settings" : {
                        "author" : "Sam Jones",
                        "date" : "1483918715021",
                        "category" : "Bass",
                        "name" : "Odyssey Bass Patch",
                        "locked" : true
                    },
                    "vco1" : {
                        "course" : 10,
                        "fine" : -200,
                        "kybd" : "Off",
                        "fm1Amt" : 50,
                        "fm1Src" : "Square",
                        "fm2Amt" : 20,
                        "fm2Src" : "ADSR",
                        "width" : 23,
                        "mod" : 75,
                        "pwSrc" : "ADSR"
                    },
                    "vco2" : {
                        "course" : 1000,
                        "fine" : 200,
                        "sync" : "ON",
                        "fm1Amt" : 50,
                        "fm1Src" : "S/H MIXER OR PEDAL",
                        "fm2Amt" : 25,
                        "fm2Src" : "ADSR",
                        "width" : 39,
                        "mod" : 50,
                        "pwSrc" : "ADSR"
                    },
                    "lfoFreq" : 20,
                    "sHold" : {
                        "mx1Amt" : 50,
                        "mx1Src" : 'VCO-1 SQUARE',
                        "mx2Amt" : 25,
                        "mx2Src" : 'VCO-2 SQUARE',
                        "trigSrc" : 'KYBD TRIG',
                        "lag" : 50
                    }
                },
                {
                    "settings" : {
                        "author" : "Eddie Van Halen",
                        "date" : "1483918715021",
                        "category" : "Lead",
                        "name" : "Odyssey Lead Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Chick Corea",
                        "date" : "1483918715021",
                        "category" : "None",
                        "name" : "Odyssey Keys Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Vangelis",
                        "date" : "1483918715021",
                        "category" : "None",
                        "name" : "Odyssey Pad Patch",
                        "locked" : true
                    }
                }
            ]
        };
    }]);

})();