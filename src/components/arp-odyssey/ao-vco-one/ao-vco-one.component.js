(function () {

    'use strict';

    angular.module('app').component('aoVcoOne', {
        templateUrl: 'components/arp-odyssey/ao-vco-one/ao-vco-one.html',
        controller: 'aoVco1Controller',
        bindings: {
            current: '<'
        }
    });
    
})();