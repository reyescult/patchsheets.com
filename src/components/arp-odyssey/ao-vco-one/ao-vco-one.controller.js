(function () {

    'use strict';

    angular.module('app').controller('aoVco1Controller', [function () {

        var vm = this;

        vm.courseHi = {
            stepsArray: [
                {value: 20, legend: '20 .2Hz'}, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 
                106, 112, 118, 125, 131, 137, 143, 150, 156, 162, 168, 175, 181, 187, 193, {value: 200, legend: '200 2'},
                250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000,
                1062, 1125, 1187, 1250, 1312, 1375, 1437, 1500, 1562, 1625, 1687, 1750, 1812, 1875, 1937, {value: 2000, legend: '2KHz 20Hz'}
            ],
            showSelectionBar: true,
            showTicks: 16, // Should be 16.25, but legends do not show up with floating value.
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.courseLo = {
            stepsArray: [
                {value: 0.2, legend: '20 .2Hz'}, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1,
                1.06, 1.12, 1.19, 1.25, 1.3, 1.37, 1.43, 1.5, 1.56, 1.62, 1.69, 1.74, 1.80, 1.87, 1.93, {value: 2, legend: '200 2'},
                2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10,
                10.63, 11.25, 11.88, 12.5, 13.13, 13.75, 14.38, 15, 15.63, 16.25, 16.88, 17.5, 18.13, 18.75, 19.36, {value: 20, legend: '2KHz 20Hz'}
            ],
            showSelectionBar: true,
            showTicks: 16, // Should be 16.25, but legends do not show up with floating value.
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.setCourse = function () {
            vm.current.vco1.course = (vm.current.vco1.kybd === 'On') ? ( vm.current.vco1.course * 100) : (vm.current.vco1.course / 100);
        }

        vm.fine = {
            floor: -400,
            ceil: 400,
            showSelectionBar: true,
            showTicks: 400,
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.fm = {
            floor: 0,
            ceil: 100,
            showSelectionBar: true,
            showTicks: 25,
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.width = {
            stepsArray: [
                {value: 50, legend: '50%'}, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 
                28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, {value: 5, legend: 'MIN'}
            ],
            showSelectionBar: true,
            showTicks: 11, // Should be 11.5, but legends do not show up with floating value.
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

        vm.mod = {
            floor: 0,
            ceil: 100,
            showSelectionBar: true,
            showTicks: 25,
            hideLimitLabels: true,
            hidePointerLabels: false,
            vertical: true
        };

    }]);
    
})();