(function () {

    'use strict';

    angular.module('app').component('aoVcoTwo', {
        templateUrl: 'components/arp-odyssey/ao-vco-two/ao-vco-two.html',
        controller: 'aoVco2Controller',
        bindings: {
            current: '<'
        }
    });
    
})();