(function () {

    'use strict';

    angular.module('app').component('aoDetails', {
        templateUrl: 'components/arp-odyssey/ao-details/ao-details.html',
        controller: 'aoDetailsController',
        bindings: {
            current: '<'
        }
    });
    
})();