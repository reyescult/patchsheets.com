(function () {

    'use strict';

    angular.module('app').controller('odysseyController', ['$scope', 'patchesService', 'odysseyPatchService', 'deviceMenuService', function ($scope, patchesService, odysseyPatchService, deviceMenuService) {
        var vm = this;
        vm.device = {
            brand: 'Arp',
            model: 'Odyssey'
        };
        vm.patches = odysseyPatchService.patches;
        vm.$onInit = function () {
            patchesService.setPatches(vm.patches);
            patchesService.setCurrent(0);
            vm.showSelections = deviceMenuService.getSelections();
            vm.showLabels = deviceMenuService.getLabels();
        };
        vm.$onDestroy = function () {
            patchesService.setPatches([]);
            patchesService.setCurrent({});
        };
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
            vm.editObj = JSON.parse(JSON.stringify(vm.current));
        });
        $scope.$on('updateDevice', function () {
            vm.showSelections = deviceMenuService.getSelections();
            vm.showLabels = deviceMenuService.getLabels();
        });
    }]);

})();