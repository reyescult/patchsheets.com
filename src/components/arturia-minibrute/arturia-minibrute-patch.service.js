(function () {

    'use strict';

    angular.module('app').service('minibruteService', [function () {
        return {
            "patches" : [
                {
                    "settings" : {
                        "author" : "Rick Reyes",
                        "date" : "1483918860904",
                        "category" : "None",
                        "name" : "Minibrute Init Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Sam Jones",
                        "date" : "1483918860904",
                        "category" : "Bass",
                        "name" : "Minibrute Bass Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Eddie Van Halen",
                        "date" : "1483918860904",
                        "category" : "Lead",
                        "name" : "Minibrute Lead Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Chick Corea",
                        "date" : "1483918860904",
                        "category" : "None",
                        "name" : "Minibrute Keys Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Vangelis",
                        "date" : "1483918860904",
                        "category" : "None",
                        "name" : "Minibrute Pad Patch",
                        "locked" : true
                    }
                }
            ]
        };
    }]);

})();