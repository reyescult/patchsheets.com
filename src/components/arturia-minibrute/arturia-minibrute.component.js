(function () {

    'use strict';

    angular.module('app').component('arturiaMinibrute', {
        templateUrl: 'components/arturia-minibrute/arturia-minibrute.html',
        controller: 'minibruteController'
    });
    
})();