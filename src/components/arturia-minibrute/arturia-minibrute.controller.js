(function () {

    'use strict';

    angular.module('app').controller('minibruteController', ['$scope', 'patchesService', 'minibruteService', function ($scope, patchesService, minibruteService) {
        var vm = this;
        vm.device = {
            brand: 'Arturia',
            model: 'Minibrute'
        };
        vm.patches = minibruteService.patches;
        vm.$onInit = function () {
            patchesService.setPatches(vm.patches);
            patchesService.setCurrent(0);
        };
        vm.$onDestroy = function () {
            patchesService.setPatches([]);
            patchesService.setCurrent({});
        };
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
            vm.editObj = JSON.parse(JSON.stringify(vm.current));
        });
    }]);

})();