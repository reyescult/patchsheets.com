(function () {

    'use strict';

    angular.module('app').service('patchesService', ['$rootScope', function ($rootScope) {
        var patches = [];
        var current = {};
        return {
            getPatches: function () {
                return patches;
            },
            setPatches: function (value) {
                patches = value;
                $rootScope.$broadcast('updatePatches');
            },
            getCurrent: function () {
                return current;
            },
            setCurrent: function (value) {
                current = patches.length ? patches[value] : {};
                $rootScope.$broadcast('updateCurrent');
            }
        }
    }]);

})();