(function () {

    'use strict';

    angular.module('app').controller('patchesController', ['$scope', 'patchesService', function ($scope, patchesService) {
        var vm = this;
        vm.patches = patchesService.getPatches();
        vm.current = patchesService.getCurrent();
        vm.setCurrent = patchesService.setCurrent;
        $scope.$on('updatePatches', function () {
            vm.patches = patchesService.getPatches();
        });
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
        });
    }]);

})();