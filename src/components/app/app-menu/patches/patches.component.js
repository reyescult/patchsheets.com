(function () {

    'use strict';

    angular.module('app').component('patches', {
        templateUrl: 'components/app/app-menu/patches/patches.html',
        controller: 'patchesController'
    });

})();