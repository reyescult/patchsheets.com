(function () {

    'use strict';

    angular.module('app').component('devices', {
        templateUrl: 'components/app/app-menu/devices/devices.html',
        controller: 'devicesController'
    });

})();