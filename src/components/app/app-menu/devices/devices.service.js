(function () {

    'use strict';

    angular.module('app').service('devicesService', function () {
        return {
            "devices" : [
                {
                    "name" : "Arp Odyssey",
                    "path" : "/arp-odyssey",
                    "type" : "Synthesizer",
                    "enabled" : true
                },
                {
                    "name" : "Arturia Minibrute",
                    "path" : "/arturia-minibrute",
                    "type" : "Synthesizer",
                    "enabled" : true
                },
                {
                    "name" : "Sequencial Pro One",
                    "path" : "/sequencial-pro-one",
                    "type" : "Synthesizer",
                    "enabled" : true
                },
                {
                    "name" : "Yamaha CS-15",
                    "path" : "/yamaha-cs-15",
                    "type" : "Synthesizer",
                    "enabled" : true
                },
                {
                    "name" : "Warm Audio WA2A",
                    "path" : "/warm-audio-wa2a",
                    "type" : "Compressor",
                    "enabled" : true
                }
            ]
        };
    });

})();