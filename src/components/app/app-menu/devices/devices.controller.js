(function () {

    'use strict';

    angular.module('app').controller('devicesController', ['$rootScope', '$location', 'devicesService', function ($rootScope, $location, devicesService) {
        var vm = this;
        vm.devices = devicesService.devices;
        vm.current = $location.path();
        $rootScope.$on('$routeChangeSuccess', function () {
            vm.current = $location.path();
        });
    }]);

})();