(function () {

    'use strict';

    angular.module('app').component('appMenu', {
        templateUrl: 'components/app/app-menu/app-menu.html'
    });

})();