(function () {

    'use strict';

    angular.module('app').controller('appController', function () {
        var vm = this;
        vm.menuState = 'show';
        vm.setMenuState = function () {
            vm.menuState = vm.menuState === 'show' ? 'hide' : 'show'
        }
    });
    
})();