(function () {
    
    'use strict';
    
    angular.module('app').config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/home', {
                title: 'Home',
                template : '<home></home>'
            })
            .when('/arp-odyssey', {
                title: 'Arp Odyssey',
                template : '<arp-odyssey></arp-odyssey>'
            })
            .when('/arturia-minibrute', {
                title: 'Arturia Minibrute',
                template : '<arturia-minibrute></arturia-minibrute>'
            })
            .when('/sequencial-pro-one', {
                title: 'Sequencial Pro One',
                template : '<sequencial-pro-one></sequencial-pro-one>'
            })
            .when('/yamaha-cs-15', {
                title: 'Yamaha CS-15',
                template : '<yamaha-cs-15></yamaha-cs-15>'
            })
            .when('/warm-audio-wa2a', {
                title: 'Warm Audio WA2A',
                template : '<warm-audio-wa2a></warm-audio-wa2a>'
            })
            .otherwise({
                redirectTo: '/arp-odyssey'
            });
    }]);
    
    angular.module('app').run(['$rootScope', '$route', function ($rootScope, $route) {
        $rootScope.$on('$routeChangeSuccess', function () {
            document.title = 'Patchsheets.com | ' + $route.current.title;
        });
    }]);
    
})();