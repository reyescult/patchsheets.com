(function () {

    'use strict';

    angular.module('app').component('app', {
        templateUrl: 'components/app/app.html',
        controller: 'appController'
    });
    
})();