(function () {

    'use strict';

    angular.module('app').component('appHeader', {
        templateUrl: 'components/app/app-header/app-header.html',
        bindings: {
            menuState: '<',
            setMenuState: '<'
        }
    });
    
})();