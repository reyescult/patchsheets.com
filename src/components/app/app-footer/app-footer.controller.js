(function () {

    'use strict';

    angular.module('app').controller('appFooterController', [function () {
        var vm = this;
        vm.year = new Date().getFullYear();
    }]);

})();