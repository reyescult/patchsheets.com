(function () {

    'use strict';

    angular.module('app').component('appFooter', {
        templateUrl: 'components/app/app-footer/app-footer.html',
        controller: 'appFooterController'
    });
    
})();