(function () {

    'use strict';

    angular.module('app').controller('homeCtrl', [function () {
        var vm = this;
        vm.title = 'Welcome to Patchsheets.com.';
        vm.description = 'Please select a device to get started';
    }]);

})();