(function () {

    'use strict';

    angular.module('app').component('sequencialProOne', {
        templateUrl: 'components/sequencial-pro-one/sequencial-pro-one.html',
        controller: 'proOneController'
    });
    
})();