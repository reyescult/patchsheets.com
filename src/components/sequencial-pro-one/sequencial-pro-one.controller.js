(function () {

    'use strict';

    angular.module('app').controller('proOneController', ['$scope', 'patchesService', 'proOnePatchService', function ($scope, patchesService, proOnePatchService) {
        var vm = this;
        vm.device = {
            brand: 'Sequencial',
            model: 'Pro One'
        };
        vm.patches = proOnePatchService.patches;
        vm.$onInit = function () {
            patchesService.setPatches(vm.patches);
            patchesService.setCurrent(0);
        };
        vm.$onDestroy = function () {
            patchesService.setPatches([]);
            patchesService.setCurrent({});
        };
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
            vm.editObj = JSON.parse(JSON.stringify(vm.current));
        });
    }]);

})();