(function () {

    'use strict';

    angular.module('app').service('proOnePatchService', [function () {
        return {
            "patches" : [
                {
                    "settings" : {
                        "author" : "Rick Reyes",
                        "date" : "1483918898191",
                        "category" : "None",
                        "name" : "Pro One Init Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Sam Jones",
                        "date" : "1483918898191",
                        "category" : "Bass",
                        "name" : "Pro One Bass Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Eddie Van Halen",
                        "date" : "1483918898191",
                        "category" : "Lead",
                        "name" : "Pro One Lead Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Chick Corea",
                        "date" : "1483918898191",
                        "category" : "None",
                        "name" : "Pro One Keys Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Vangelis",
                        "date" : "1483918898191",
                        "category" : "None",
                        "name" : "Pro One Pad Patch",
                        "locked" : true
                    }
                }
            ]
        };
    }]);

})();