(function () {

    'use strict';

    angular.module('app').component('deviceHeader', {
        templateUrl: 'components/device-header/device-header.html',
        controller: 'deviceHeaderController',
        bindings: {
            device: '<',
            current: '<'
        }
    });
    
})();