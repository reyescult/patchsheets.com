(function () {

    'use strict';

    angular.module('app').component('deviceMenu', {
        templateUrl: 'components/device-menu/device-menu.html',
        controller: 'deviceMenuController',
        bindings: {
            
        }
    });
    
})();