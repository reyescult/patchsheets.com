(function () {

    'use strict';

    angular.module('app').controller('deviceMenuController', ['$scope', 'deviceMenuService', function ($scope, deviceMenuService) {
        var vm = this;
        vm.setSelections = deviceMenuService.setSelections;
        vm.setLabels = deviceMenuService.setLabels;
        vm.$onInit = function () {
            vm.showSelections = deviceMenuService.getSelections();
            vm.showLabels = deviceMenuService.getLabels();
        };
        $scope.$on('updateDevice', function () {
            vm.showSelections = deviceMenuService.getSelections();
            vm.showLabels = deviceMenuService.getLabels();
        });
    }]);

})();