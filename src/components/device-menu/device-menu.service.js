(function () {

    'use strict';

    angular.module('app').service('deviceMenuService', ['$rootScope', function ($rootScope) {
        var selections = true;
        var labels = true;
        return {
            getSelections: function () {
                return selections;
            },
            setSelections: function () {
                selections = !selections;
                $rootScope.$broadcast('updateDevice');
            },
            getLabels: function () {
                return labels;
            },
            setLabels: function () {
                labels = !labels;
                $rootScope.$broadcast('updateDevice');
            }
        };
    }]);

})();