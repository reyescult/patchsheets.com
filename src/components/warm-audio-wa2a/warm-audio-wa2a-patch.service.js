(function () {

    'use strict';

    angular.module('app').service('wa2aPatchService', [function () {
        return {
            "patches" : [
                {
                    "settings" : {
                        "author" : "Rick Reyes",
                        "date" : "1483918993082",
                        "category" : "None",
                        "name" : "WA2A Init Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Sam Jones",
                        "date" : "1483918993082",
                        "category" : "Bass",
                        "name" : "WA2A Bass Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Eddie Van Halen",
                        "date" : "1483918993082",
                        "category" : "Lead",
                        "name" : "WA2A Lead Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Chick Corea",
                        "date" : "1483918993082",
                        "category" : "None",
                        "name" : "WA2A Keys Patch",
                        "locked" : true
                    }
                },
                {
                    "settings" : {
                        "author" : "Vangelis",
                        "date" : "1483918993082",
                        "category" : "None",
                        "name" : "WA2A Pad Patch",
                        "locked" : true
                    }
                }
            ]
        };
    }]);

})();