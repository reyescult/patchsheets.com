(function () {

    'use strict';

    angular.module('app').component('warmAudioWa2a', {
        templateUrl: 'components/warm-audio-wa2a/warm-audio-wa2a.html',
        controller: 'wa2aController'
    });
    
})();