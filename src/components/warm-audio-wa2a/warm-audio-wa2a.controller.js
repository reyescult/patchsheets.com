(function () {

    'use strict';

    angular.module('app').controller('wa2aController', ['$scope', 'patchesService', 'wa2aPatchService', function ($scope, patchesService, wa2aPatchService) {
        var vm = this;
        vm.device = {
            brand: 'Warm Audio',
            model: 'WA2A'
        };
        vm.patches = wa2aPatchService.patches;
        vm.$onInit = function () {
            patchesService.setPatches(vm.patches);
            patchesService.setCurrent(0);
        };
        vm.$onDestroy = function () {
            patchesService.setPatches([]);
            patchesService.setCurrent({});
        };
        $scope.$on('updateCurrent', function () {
            vm.current = patchesService.getCurrent();
            vm.editObj = JSON.parse(JSON.stringify(vm.current));
        });
    }]);

})();